/*
Michael Ashmead, 600.120, 3/6/15, Homework 4, 484-682-9355, mashmea1, ashmeadmichael@gmail.com
*/

#define DEFAULT_INPUT_LENGTH 200
#define DEFAULT_TITLE_LENGTH 30

typedef struct {
   char *division;
   int department;
   int courseNum;
   int credits1;
   int credits2;
   char *name;
} courseInfo;
struct node {
   struct node *next;
   courseInfo info;
   char grade[3];
};

#include <stdio.h>

void prompt();
courseInfo parseCourse(FILE *file);
void readFileOfCourses(char input[]);

int main(int argc, char* argv[]) {
   //check to make sure user entered a database file name
   if (argc != 2) {
      printf("Please enter a database file name!\n");
      return 1;
   }

   //check if database file exits
   FILE *file = fopen(argv[1], "r");
   if (file == NULL) //the file doesn't exist, so create the file
      fclose(fopen(argv[1], "w"));

   file = fopen(argv[1], "a+");

   //prompt for user input
   char input[DEFAULT_INPUT_LENGTH];
   do {
      prompt();
      scanf("%s", input);
      switch (input[0]) {
         case '1':
            readFileOfCourses(input);
      }
   } while(input[0] != '0');

   return 0;
}

void prompt() {
   printf("Enter a number: ");
}

//parse the course information and store it all into a courseInfo struct
courseInfo parseCourse(FILE *file) {
   int c1, c2;
   char div[3], title[DEFAULT_TITLE_LENGTH + 1];
   int dept, num;
   int count = 0;
   int c = ' ';
   fscanf(file, "%2s.%d.%d %d.%d ", div, &dept, &num, &c1, &c2);
   while (count < DEFAULT_TITLE_LENGTH && (c = fgetc(file)) != '\n') {
      title[count++] = (char) c;
   }
   title[count] = '\0';
   while (c != '\n')
      c = fgetc(file); //skip to end of line

   //store info into courseInfo struct
   courseInfo ci;
   ci.division = div;
   ci.department = dept;
   ci.courseNum = num;
   ci.credits1 = c1;
   ci.credits2 = c2;
   ci.name = title;

   return ci;
}

void readFileOfCourses(char input[]) {

}
