# Makefile for HW4
#
# USAGE:
#
# // to compile:
# make
#
# // to compile tests and run tests:
# make test
#
# // remove compilation output files:
# make clean
#

# make variables let us avoid pasting these options in multiple places
CC = gcc 
CFLAGS = -std=c99 -Wall -Wextra -pedantic -O         # for final build
#CFLAGS = -std=c99 -Wall -Wextra -pedantic -O0 -g   # for debugging

bin: hw4

test: test_database
	@echo "Running tests..."
	./test_database
	@echo "All Tests Passed."

database.o: database.c database.h
	$(CC) $(CFLAGS) -c database.c

test_database.o: test_database.c database.h
	$(CC) $(CFLAGS) -c test_database.c

hw4.o: hw4.c database.h
	$(CC) $(CFLAGS) -pedantic -O -c hw4.c

test_database: test_database.o database.o
	$(CC) $(CFLAGS) -O -o test_database test_database.o database.o

hw4: hw4.o database.o
	$(CC) $(CFLAGS) -O -o hw4 hw4.o database.o

clean:
	rm -f *.o test_database hw4