How the program works:
1) Take in input from stdin that specifies the database file
2) If the database file can't be found, the program will initialize a database file with 560,000 blank courses.
   - A blank course has "" for its division, 000 for its department number and 000 for its course number, 0 for credits, and "" for its course name
3) If the database file exists, open it for reading and writing.
4) Initialize 10 linked lists.
5) Wait for user input.